﻿using Akka.State.MatchHandler;

namespace Akka.State;

public abstract class ActorState
{
    private readonly MatchBuilder _matchHandlerBuilder;
    private PartialAction<object> _partialHandler = _ => false;

    protected ActorState()
    {
        _matchHandlerBuilder = new MatchBuilder(CachedMatchCompiler<object>.Instance);
    }

    protected void InitialiseState()
    {
        _partialHandler = _matchHandlerBuilder.Build();
    }

    protected void Handle<T>(Action<T> handler, Predicate<T> shouldHandle = null!)
    {
        _matchHandlerBuilder.Match<T>(handler, shouldHandle);
    }

    public bool Raise(IStateEvent stateEvent, Action? continueWith = null)
    {
        bool wasHandled = _partialHandler(stateEvent);

        if (!wasHandled) throw new ArgumentException("An unhandled event was raised");

        if (continueWith != null) continueWith();

        return true;
    }
}