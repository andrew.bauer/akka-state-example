﻿//-----------------------------------------------------------------------
// <copyright file="IMatchExpressionBuilder.cs" company="Akka.NET Project">
//     Copyright (C) 2009-2022 Lightbend Inc. <http://www.lightbend.com>
//     Copyright (C) 2013-2022 .NET Foundation <https://github.com/akkadotnet/akka.net>
// </copyright>
//-----------------------------------------------------------------------

namespace Akka.State.MatchHandler
{
    /// <summary>
    /// TBD
    /// </summary>
    internal interface IMatchExpressionBuilder
    {
        /// <summary>
        /// TBD
        /// </summary>
        /// <param name="typeHandlers">TBD</param>
        /// <returns>TBD</returns>
        MatchExpressionBuilderResult BuildLambdaExpression(IReadOnlyList<TypeHandler> typeHandlers);

        /// <summary>
        /// TBD
        /// </summary>
        /// <param name="arguments">TBD</param>
        /// <returns>TBD</returns>
        object[] CreateArgumentValuesArray(IReadOnlyList<Argument> arguments);
    }
}

