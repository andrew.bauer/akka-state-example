namespace AkkaStateExample.Test
{
    public class ClientStateTests
    {
        [Fact]
        public void State_Initialisation()
        {
            var clientKey = new ClientKey(Guid.NewGuid());
            var state = new ClientState(clientKey);

            state.ClientKey.ClientId.Should().Be(clientKey.ClientId);
            state.Status.Should().Be(ClientStatus.Initialising);
        }

        [Fact]
        public void ClientActivated_StateEvent()
        {
            var state = new ClientState(new ClientKey(Guid.NewGuid()));

            state.Raise(new ClientStateEvents.ClientActivated());

            state.Status.Should().Be(ClientStatus.Active);
        }

        [Fact]
        public void ClientArchived_StateEvent()
        {
            var state = new ClientState(new ClientKey(Guid.NewGuid()));

            state.Raise(new ClientStateEvents.ClientArchived());

            state.Status.Should().Be(ClientStatus.Archived);
        }

        [Fact]
        public void ClientMessageReceived_StateEvent()
        {
            var state = new ClientState(new ClientKey(Guid.NewGuid()));

            state.Raise(new ClientStateEvents.ClientActivated());
            state.Raise(new ClientStateEvents.ClientMessageReceived("test message"));

            state.ClientMessages.Count.Should().Be(1);
            state.ClientMessages[0].Should().BeEquivalentTo("test message");
        }

        [Fact]
        public void ClientMessageReceived_StateEvent_When_Status_Not_Active_Should_Fail()
        {
            var state = new ClientState(new ClientKey(Guid.NewGuid()));

            var raiseResult = () => state.Raise(new ClientStateEvents.ClientMessageReceived("test message"));

            raiseResult.Should().Throw<InvalidOperationException>();
        }
    }
}