# Akka State Example



## Name
Akka.State Example

## Description
The actor model does a very good job at completely encapsulating state and behaviour into a single "object". It achieves this by having a boundary that another actor (or object) cannot directly cross. The only way of getting access to the state and behaviour is through passing the actor a message.

With a standard actor, when the actor dies, the state and behaviour goes with it. When you want to persist state past an actor's death (using something like a PersistentActor), a new encapsulation layer needs to be added around the state itself. This is where the Event Sourcing pattern is usually applied - where state-mutation only occurs based on an event being raised with that state.

Akka.NET does a fabuluous job at being able to reconstitute PersistentActors by rebuilding the state through replaying of the events that occurred before the actor's death, however it leaves the structuring of the state, and mutation of the state upon receiving events, up to you. This project demonstrates a state structure that uses the same principles of how a ReceiveActor works - registering functions that will mutate the state when an event is "Raised" with it.

This approach separates an actor's state-mutation functionality from its behavioural functionality, and has a few benefits:
* Becomes easier to reason with state-mutation
* Becomes easier to apply and replay events
* More percise unit-tests can be created specifically around the state-mutation functionality

## License
I am not up-to-speed with open source licences, so will just state it simply, you are free to use it at your own risk and responsibility. The MatchHandler functionality is an Akka.NET internal utility that was copied as is - its copyrights & licences remain as per those applied to it from the Akka.NET Project.

## Usage
As per the licence, feel free to use this within your projects at your risk and responsibility. Keep in mind that the MatchHandler functionality is a copy from the Akka.NET internals. Should that functionality change within Akka.NET, this copy will obviously become out of sync with those internals.

At the end of your constructor (after all your event-handle methods have been added), you need to run InitialiseState().

There is a docker-compose file that will start an SQL Server for persisting the example actor. Make sure to create an "Akka" database before running the example.