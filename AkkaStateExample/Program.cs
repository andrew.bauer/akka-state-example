﻿using Akka.Actor;
using Akka.Cluster.Hosting;
using Akka.Cluster.Sharding;
using Akka.Hosting;
using Akka.Persistence.SqlServer.Hosting;
using Akka.Remote.Hosting;
using AkkaStateExample;
using Microsoft.AspNetCore.Mvc;

string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddEnvironmentVariables();

IConfigurationSection akkaConfig = builder.Configuration.GetSection("Akka");
string hostname = akkaConfig.GetValue("ClusterIp", "localhost");
int port = akkaConfig.GetValue("ClusterPort", 7900);
Address[] seeds = akkaConfig.GetValue("ClusterSeeds", new[] { "akka.tcp://akka-state-example@localhost:7900" }).Select(Address.Parse).ToArray();
string clusterPersistenceConnectionString = builder.Configuration.GetConnectionString("AkkaSqlConnection");

builder.Services.AddAkka("akka-state-example", (configurationBuilder, _) =>
    {
        configurationBuilder
            .WithRemoting(hostname, port)
            .WithClustering(new ClusterOptions() { Roles = new[] { "EntityRole" }, SeedNodes = seeds })
            .WithSqlServerPersistence(clusterPersistenceConnectionString)
            .WithShardRegion<ClientMarker>("clients", ClientActor.CreateProps,
                new ClientMessageRouter(),
                new ShardOptions
                {
                    RememberEntities = false,
                    Role = "EntityRole",
                    StateStoreMode = StateStoreMode.Persistence
                });
    })
    .AddHostedService<AkkaService>();

var app = builder.Build();

app.MapGet("/", () => {});

app.MapPost("/client/{clientId:guid}", (Guid clientId, ActorRegistry registry) =>
{
    var clientShard = registry.Get<ClientMarker>();
    clientShard.Tell(new ClientMessageProtocol.ActivateClient(new ClientKey(clientId)));
});

app.MapPost("/client/{clientId:guid}/message", (Guid clientId, [FromBody] string message, ActorRegistry actorRegistry) =>
{
    var clientShard = actorRegistry.Get<ClientMarker>();
    clientShard.Tell(new ClientMessageProtocol.HandleClientMessage(new ClientKey(clientId), message));
});

app.MapGet("/client/{clientId:guid}/messages", async (Guid clientId, ActorRegistry actorRegistry) =>
{
    var clientShard = actorRegistry.Get<ClientMarker>();
    return await clientShard.Ask<ClientMessageProtocol.GetClientMessagesResponse>(new ClientMessageProtocol.GetClientMessagesQuery(new ClientKey(clientId)), TimeSpan.FromSeconds(3));
});

app.Run();