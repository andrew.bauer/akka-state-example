﻿using System.Collections.Immutable;
using Akka.Actor;
using Akka.Persistence;
using Akka.State;

namespace AkkaStateExample
{
    public class ClientActor : ReceivePersistentActor
    {
        public const string ClientEntityNameConstant = "client";
        public override string PersistenceId { get; }

        private ClientState State { get; }

        public ClientActor(string clientIdentifier)
        {
            PersistenceId = $"{ClientEntityNameConstant}_{clientIdentifier}";

            if (!Guid.TryParse(clientIdentifier, out Guid clientId))
            {
                clientId = Guid.NewGuid();
            }

            State = new ClientState(new ClientKey(clientId));

            Recover<IStateEvent>(stateEvent => State.Raise(stateEvent));
        }

        public static Props CreateProps(string clientIdentifier)
        {
            return Props.Create<ClientActor>(clientIdentifier);
        }

        protected override void OnReplaySuccess()
        {
            ChangeClientStatus(State.Status);
        }

        private void Initialising()
        {
            Command<ClientMessageProtocol.ActivateClient>(_ =>
            {
                var stateEvent = new ClientStateEvents.ClientActivated();
                Persist(stateEvent, _ => State.Raise(stateEvent, () => ChangeClientStatus(ClientStatus.Active)));
            });
        }

        private void Active()
        {
            Command<ClientMessageProtocol.HandleClientMessage>(msg =>
            {
                var stateEvent = new ClientStateEvents.ClientMessageReceived(msg.Message);
                Persist(stateEvent, _ => State.Raise(stateEvent));
            });

            Command<ClientMessageProtocol.GetClientMessagesQuery>(_ =>
            {
                Sender.Tell(new ClientMessageProtocol.GetClientMessagesResponse(State.ClientKey, State.ClientMessages));
            });

            Command<ClientMessageProtocol.ArchiveClient>(_ =>
            {
                var stateEvent = new ClientStateEvents.ClientArchived();
                Persist(stateEvent, _ => State.Raise(stateEvent, () => ChangeClientStatus(ClientStatus.Archived)));
            });
        }

        private void Archived() {}

        private void ChangeClientStatus(ClientStatus clientStatus)
        {
            switch (clientStatus)
            {
                case ClientStatus.Active:
                    Become(Active);
                    break;
                case ClientStatus.Archived:
                    Become(Archived);
                    break;
                case ClientStatus.Initialising:
                default:
                    Become(Initialising);
                    break;
            }
        }
    }

    public interface IWithClientKey
    {
        public ClientKey ClientKey { get; }
    }

    public class ClientMessageProtocol
    {
        public record ActivateClient(ClientKey ClientKey) : IWithClientKey;
        public record HandleClientMessage(ClientKey ClientKey, string Message) : IWithClientKey;
        public record GetClientMessagesQuery(ClientKey ClientKey) : IWithClientKey;
        public record GetClientMessagesResponse(ClientKey ClientKey, ImmutableList<string> Messages) : IWithClientKey;
        public record ArchiveClient(ClientKey ClientKey) : IWithClientKey;
    }
}
