﻿using Akka.Actor;

namespace AkkaStateExample
{
    public sealed class AkkaService : IHostedService
    {
        private readonly ActorSystem _system;

        public AkkaService(ActorSystem system)
        {
            _system = system;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
