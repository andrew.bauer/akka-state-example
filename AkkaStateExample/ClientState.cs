﻿using System.Collections.Immutable;
using Akka.State;

namespace AkkaStateExample
{
    public class ClientState : ActorState
    {
        public ClientKey ClientKey { get; init; }
        public ClientStatus Status { get; private set; }
        public ImmutableList<string> ClientMessages { get; private set; }

        public ClientState(ClientKey clientKey)
        {
            ClientKey = clientKey;
            Status = ClientStatus.Initialising;
            ClientMessages = new List<string>().ToImmutableList();

            Handle<ClientStateEvents.ClientActivated>(_ =>
            {
                Status = ClientStatus.Active;
            });

            Handle<ClientStateEvents.ClientMessageReceived>(stateEvent =>
            {
                if (Status != ClientStatus.Active) throw new InvalidOperationException("Cannot receive a message until the Client is Active.");
                ClientMessages = ClientMessages.Add(stateEvent.Message);
            });

            Handle<ClientStateEvents.ClientArchived>(_ =>
            {
                Status = ClientStatus.Archived;
            });

            InitialiseState();
        }
    }

    public class ClientStateEvents
    {
        public record ClientActivated : IStateEvent;
        public record ClientMessageReceived(string Message) : IStateEvent;
        public record ClientArchived : IStateEvent;
    }

    public enum ClientStatus
    {
        Initialising,
        Active,
        Archived
    }
}
