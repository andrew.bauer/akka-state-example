﻿namespace AkkaStateExample
{
    public record ClientKey(Guid ClientId);
}
