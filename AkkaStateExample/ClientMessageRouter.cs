﻿using Akka.Cluster.Sharding;

namespace AkkaStateExample
{
    public class ClientMessageRouter : HashCodeMessageExtractor
    {
        public ClientMessageRouter() : base(10) {}  // using 1 node with 10 shards

        public override string? EntityId(object message)
        {
            if (message is IWithClientKey clientMessage)
            {
                string clientIdentifier = clientMessage.ClientKey.ClientId.ToString();
                return clientIdentifier;
            }

            return null;
        }
    }
}
